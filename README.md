# Flash Cards Team #1

This repo is for team #1. We are going to be #1

### Implemented Features
* Registering
* Local storage
* Editing decks / Collections
* Basic Soloplayer functionality
* Offline Mode

### Planned Features
* Community tab / online tab to see other players collections and download them

#### Backend Repo can be found here [https://bitbucket.org/KBlock47/flashcardsbackend/src/master/](https://bitbucket.org/KBlock47/flashcardsbackend/src/master/)