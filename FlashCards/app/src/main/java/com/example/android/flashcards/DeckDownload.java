package com.example.android.flashcards;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.android.flashcards.classes.Card;
import com.example.android.flashcards.classes.CollectionAdapter;
import com.example.android.flashcards.classes.Deck;
import com.example.android.flashcards.classes.DeckAdapter;
import com.example.android.flashcards.classes.Fetcher;
import com.example.android.flashcards.classes.OnlineDeck;
import com.example.android.flashcards.classes.SimpleREST;
import com.example.android.flashcards.interfaces.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DeckDownload extends AppCompatActivity {
    private RecyclerView.Adapter recycleAdapter;


    public void RefreshAdapter(List<OnlineDeck> decksToShow){
        if (recycleAdapter != null) {
            ((DeckAdapter) recycleAdapter).DeleteData();
        }

        recycleAdapter = new DeckAdapter(decksToShow);
        RecyclerView recycleView = findViewById(R.id.onlineDeckView);
        recycleView.setAdapter(recycleAdapter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_download);

        final Context cContext = this;

        RecyclerView recycleView = findViewById(R.id.onlineDeckView);
        recycleView.setLayoutManager(new LinearLayoutManager(this));

        SimpleREST fetchCollections = new SimpleREST(this);
        try {
            fetchCollections.GetJSON(Fetcher.FetchCollections(), new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) throws JSONException {
                    JSONArray data = new JSONArray(result);

                    ArrayList<OnlineDeck> decksToShow = new ArrayList<>();

                    for(int k = 0;k < data.length(); k++){
                        JSONObject deckInfo = (JSONObject) data.get(k);

                        int collectionid = (int) deckInfo.get("collectionid");
                        int deckAmount = (int) deckInfo.get("amountofcards");

                        if (deckAmount <= 0){
                            continue;
                        }

                        String deckName = (String) deckInfo.get("collectionname");
                        String deckAuthor = (String) deckInfo.get("displayname");

                        OnlineDeck deckData = new OnlineDeck(collectionid,deckAmount,deckName,deckAuthor);
                        decksToShow.add(deckData);
                    }

                    RefreshAdapter(decksToShow);
                }

                @Override
                public void onFailure(String result) {
                    Toast.makeText(cContext,"Error when fetching collections",Toast.LENGTH_SHORT).show();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void DownloadDeck(final OnlineDeck deckinfo){
        final int collectionid = deckinfo.GetCollectionID();
        Log.d("Collectindi ",String.valueOf(collectionid));

        SimpleREST fetch = new SimpleREST(this);
        final Context cContext = this;
        try {
            fetch.GetJSON(Fetcher.DownloadURL(collectionid), new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) throws JSONException {
                    JSONArray downloadedDeck = new JSONArray(result);
                    ArrayList<Card> cards = new ArrayList<>();

                    for(int k = 0; k < downloadedDeck.length(); k++){
                        JSONObject cardData = (JSONObject) downloadedDeck.get(k);

                        Card coolCard = new Card(cardData.getString("fronttext"),cardData.getString("backtext"));
                        cards.add(coolCard);
                    }

                    String deckName = deckinfo.GetDeckName();

                    Deck newDeck = new Deck(deckName+"-"+String.valueOf(collectionid),cards);
                    newDeck.save(cContext);
                    Toast.makeText(cContext,"Deck "+deckName+" saved!",Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(String result) {
                    Toast.makeText(cContext,"Error when downloading deck",Toast.LENGTH_SHORT).show();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
