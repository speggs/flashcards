package com.example.android.flashcards;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.android.flashcards.classes.User;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toast.makeText(this,"Welcome back " + User.LoggedIn.GetName(),Toast.LENGTH_LONG).show();
    }

    public void onPlaySoloClicked(View v){
        Intent decks = new Intent(getApplicationContext(), DecksActivity.class);
        decks.putExtra("selecting",true);
        startActivity(decks);
    }

    public void onPlayMulitplayerClicked(View v){
        Intent playMulti = new Intent(getApplicationContext(), MultiplayerActivity.class);
        startActivity(playMulti);
    }

    public void onProfileClicked(View v){
        Intent profile = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(profile);
    }

    public void onSettingsClicked(View v){
        Intent settings = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(settings);
    }

    public void onDecksClicked(View v){
        Intent decks = new Intent(getApplicationContext(), DecksActivity.class);
        startActivity(decks);
    }

    public void onDownloadClicked(View v){
        Intent decks = new Intent(getApplicationContext(), DeckDownload.class);
        startActivity(decks);
    }

    public void onLogout(View v){

        String loggedInEmail = User.LoggedIn.GetEmail();

        User.Logout(this,loggedInEmail);
    }
}
