package com.example.android.flashcards.classes;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by SPeggs on 3/22/2018.
 * This class extends applicaton to allow getting and setting of global variables
 */

public class FCApplication extends Application {
    private ArrayList<Deck> decks = new ArrayList<>();
    private int focused = -1;
    private int gamesPlayed = 0;
    private int gamesWon = 0;

/*
 *  Goes into the application's private directory, scans for and loads .deck files
 */
    public void loadDecks(){
        //TODO move save directory to 'My Documents/Flashcards'
        String[] filelist = fileList();
        for(int i = 0; i < filelist.length; i++){
            if(filelist[i].endsWith(".deck")){
                Deck deck = new Deck();
                deck.load(getApplicationContext(), filelist[i].substring(0,filelist[i].length()-5));
                decks.add(deck);
            }
        }
    }
    /*
    *   Return deck at index i
     */
    public Deck getDeck(int i){
        if(i <= 0 && i < decks.size()){
            return decks.get(i);
        }else{
            Toast.makeText(this, "Select Valid Deck", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public void addDeck(Deck d){
        decks.add(d);
    }

    /*
    * Delete deck file and remove it from this structure
     */
    public void deleteDeck(int i){
        if(i <= 0 && i < decks.size()){
            decks.get(i).deleteDeck(getApplicationContext());
            decks.remove(i);
        }else{
            Toast.makeText(this, "Select Valid Deck", Toast.LENGTH_LONG).show();
        }
    }


    public int getNumDecks(){
        return decks.size();
    }

    public int getNumCards(){
        int sum = 0;
        for(int i = 0; i < decks.size();i++){
            sum += decks.get(i).getSize();
        }
        return sum;
    }

    public void incrementGamesWon(){
        gamesWon++;
    }

    public int getGamesWon(){
        return gamesWon;
    }

    public void incrementGamesPlayed(){
        gamesPlayed++;
    }

    public int getGamesPlayed(){
        return gamesPlayed;
    }

    public int getFocused(){
        return focused;
    }

    public void setFocused(int i){
        focused = i;
    }
}
