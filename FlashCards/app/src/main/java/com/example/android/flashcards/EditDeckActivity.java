package com.example.android.flashcards;

/*
* Speggs
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.flashcards.classes.Card;
import com.example.android.flashcards.classes.CollectionAdapter;
import com.example.android.flashcards.classes.Deck;
import com.example.android.flashcards.classes.User;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/*This activity is for viewing and manipulating a Deck as a list of cards,
 */
//TODO when editing the deck name the 'done' button should dismiss the on screen keyboard
//card numbers should probably start with 1 for our users
public class EditDeckActivity extends AppCompatActivity {

    private int focused = 0;
    public Deck deck;
    String deckName;

    private RecyclerView.Adapter recycleAdapter;

    //TODO: Make this less dumb as you shouldn't have to reset the adapter but the view was updating too slow without it and may throw an index out of range exception
    public void RefreshAdapter(){
        if (recycleAdapter != null){
            ((CollectionAdapter)recycleAdapter).DeleteData();
        }
        List<Card> arr = new ArrayList<>();

        for(int i = 0; i < deck.getSize(); i++){
            Card c = deck.getCard(i);
            if (c != null){
                arr.add(c);
            }
        }
        recycleAdapter = new CollectionAdapter(arr);
        RecyclerView recycleView = findViewById(R.id.editDeckListView);
        recycleView.setAdapter(recycleAdapter);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_deck);

        String sCard = "";
        String sFront = "";
        String sBack = "";



        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                deckName= null;
            } else {
                deckName= extras.getString("deckName");
                sCard = extras.getString("sCard");
                sFront = extras.getString("front");
                sBack = extras.getString("back");
            }
        } else {
            deckName= (String) savedInstanceState.getString("deckName");
            sCard= (String) savedInstanceState.getSerializable("deckName");
        }

        deck = new Deck();
        //TODO this seems like alot of unnecessary loading and unloading to communicate between activities
        //perhaps a global ArrayList of Decks, or the passage of a single open file handle? low priority
        deck.load(this, deckName);
        EditText et = findViewById(R.id.deckNameEditText);
        et.setText(deckName);
        et.setImeOptions(EditorInfo.IME_ACTION_DONE);
        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            //TODO: Fix this. I want a DONE button not an EMOJI button
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE){
                    Log.d("Saved","");
                    String name = textView.getText().toString();
                    deck.setName(name,getApplicationContext());
                    InputMethodManager imm = (InputMethodManager) textView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        if(getIntent().hasExtra("cardnum") && getIntent().hasExtra("front") && getIntent().hasExtra("back")) {
            Bundle extras = getIntent().getExtras();
            int cardnumToEdit = extras.getInt("cardnum");

            Card editCard = null;
            if(deck.getSize() >= cardnumToEdit) {
                editCard = deck.getCard(cardnumToEdit);
            }else{
                editCard = new Card();
                deck.addCard(editCard);
            }

            if (sFront != null) {
                editCard.setFront(sFront);
            }
            if (sBack != null) {
                editCard.setBack(sBack);
            }
            deck.save(this);
        }
        RecyclerView recycleView = findViewById(R.id.editDeckListView);
        recycleView.setLayoutManager(new LinearLayoutManager(this));

        RefreshAdapter();
    }

    public void parseDeck(){

    }

    public void onReturnClicked(View v){
        Intent decks = new Intent(getApplicationContext(), DecksActivity.class);
        EditText et = findViewById(R.id.deckNameEditText);
        deck.setName(et.getText().toString(), getApplicationContext());
        decks.putExtra("deckName", deck.getName());
        startActivity(decks);
    }

    /*
    * Takes user to the edit card screen
    * Must save first in case the file name has changed
    */

    public void onEditClicked(View v){
        EditText deckNameet = findViewById(R.id.deckNameEditText);
        deck.setName(deckNameet.getText().toString(), getApplicationContext());

        Intent editCard = new Intent(getApplicationContext(), EditCardActivity.class);

        EditText et = findViewById(R.id.deckNameEditText);

        TextView questionView = v.findViewById(R.id.questionTxt);
        TextView answerView = v.findViewById(R.id.answerTxt);
        TextView cardNumView = v.findViewById(R.id.cardnum);

        String question = questionView.getText().toString();
        String answer = answerView.getText().toString();
        int cardNum = Integer.valueOf(cardNumView.getText().toString());

        editCard.putExtra("deckName", et.getText().toString());
        editCard.putExtra("front", question);
        editCard.putExtra("back", answer);
        editCard.putExtra("sCard",deck.getCard(cardNum).saveString());
        editCard.putExtra("cardnum",cardNum);
        startActivity(editCard);
    }

    public void onNewClicked(View v){
        Card newCard = new Card();
        deck.addCard(newCard);
        List<Card> help = new ArrayList<>();
        help.add(newCard);
        recycleAdapter = new CollectionAdapter(help);
        RecyclerView recycleView = findViewById(R.id.editDeckListView);
        recycleView.setAdapter(recycleAdapter);
        //populateListView();
        RefreshAdapter();
        deck.save(this);
    }

    public void onDeleteClicked(View v){
        TextView cardnumView = v.findViewById(R.id.cardnum);
        int cardnum = Integer.valueOf(cardnumView.getText().toString());
        deck.removeCard(cardnum);
        deck.save(this);

        if (recycleAdapter != null && recycleAdapter instanceof CollectionAdapter){
            RefreshAdapter();
        }
    }
}
