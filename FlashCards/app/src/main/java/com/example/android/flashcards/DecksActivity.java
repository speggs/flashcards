package com.example.android.flashcards;
/*This activity is for viewing and manipulating a list of Decks, the "view" deck button
* will allow to select, add, delete, view decks
* The activity consists of a list of selectable items, deck names. And a row of buttons which then
* operate on the currently selected list.
* -SPeggs
* */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.flashcards.classes.Card;
import com.example.android.flashcards.classes.Deck;
import com.example.android.flashcards.classes.Fetcher;
import com.example.android.flashcards.classes.SimpleREST;
import com.example.android.flashcards.classes.User;
import com.example.android.flashcards.interfaces.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class DecksActivity extends AppCompatActivity {
    //index of the currently highlighted list item, might be -1 for none selected
    private int focused = 0;
    public ArrayList<Deck> decks;
    public boolean isSelecting = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decks);
        Bundle extras = getIntent().getExtras();

        if (getIntent().hasExtra("selecting")){
            isSelecting = true;
        }
        Button newBtn = findViewById(R.id.newDeckBtn);
        newBtn.setEnabled(!isSelecting);

        decks = new ArrayList<>();
        loadDecks();
        registerClickCallBack();
    }

    private void loadDecks(){
        //TODO move save directory to 'My Documents/Flashcards'
        Context c = getApplication();
        String[] filelist = c.fileList();
        for(int i =0; i<filelist.length;i++){
            if(filelist[i].endsWith(".deck")){
                Deck deck = new Deck();
                deck.load(getApplicationContext(), filelist[i].substring(0,filelist[i].length()-5));
                //if (deck.getSize() > 0){
                //Only show decks with more than one card because why would you want an empty collection
                //Maybe you're making decks for all your class at the start of the semester, the real
                //question why shouldn't we give the users this power? -SP
                decks.add(deck);
                //}
            }
        }
        populateListView();
    }

    /*Set up list view*/
    private void populateListView(){
        //TODO make adapter draw data from a file wide arraylist
        ListView decksLV = findViewById(R.id.decksListView);
        int numDecks = decks.size();
        String[] deckNames = new String[numDecks];
        for(int i = 0; i < numDecks; i++){
            deckNames[i] = decks.get(i).deckName;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, deckNames);
        decksLV.setAdapter(adapter);
    }


/*Assign click listener*/
    private void registerClickCallBack(){
        ListView list = findViewById(R.id.decksListView);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ListView list = (ListView) view.getParent();
                TextView temp;
                //Highlight focused and unhighlight old focus
                if(focused >= 0 && focused < decks.size()) {
                    temp = (TextView) list.getChildAt(focused);
                    temp.setTextColor(Color.BLACK);
                    temp.setBackgroundColor(Color.WHITE);
                }

                focused = i;

                temp = (TextView) view;
                temp.setTextColor(Color.WHITE);
                temp.setBackgroundColor(Color.BLACK);

                if (isSelecting){
                    Intent startSolo = new Intent(getApplicationContext(), SoloPlayActivity.class);
                    startSolo.putExtra("sDeck",decks.get(i).deckName);
                    startActivity(startSolo);
                }
            }
        });
    }

    //Code stubs to be implemented later
    public void parseDecks(){

    }

    public void onNewDeck(View v){
        Deck newDeck = new Deck();
        decks.add(newDeck);
        populateListView();
    }

    public void onDeleteDeck(View v){
        if(focused >= 0 && focused < decks.size()) {
            decks.get(focused).deleteDeck(getApplicationContext());
            decks.remove(focused);
            focused = -1;
            populateListView();
        }else{
            Toast.makeText(this, "Select Valid Deck", Toast.LENGTH_LONG).show();
        }

    }

    /*
    * TODO This code is unsafe, all sorts of problems like file name conflicts
    * there must be a better way to pass objects between activities
    */
    public void onEditDeck(View v){
        Intent editDeck = new Intent(getApplicationContext(), EditDeckActivity.class);
        if(focused >= 0 && focused < decks.size()) {
            decks.get(focused).save(getApplicationContext());
            String deckName = decks.get(focused).getName();
            editDeck.putExtra("deckName", deckName);
            startActivity(editDeck);
        }
//        }else{
//            Toast.makeText(this, "Select a Valid Deck",
//                    Toast.LENGTH_LONG).show();
//        }
    }

    public void onMenuClicked(View v){
        Intent menu = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(menu);
    }

    public void onUploadClicked(View v) {
        HashMap<String, Object> userData = User.LoggedIn.GetInfo();

        if ((int) userData.get("serverid") == -1) {
            Toast.makeText(this, "You cannot upload a deck while offline!", Toast.LENGTH_LONG).show();
            return;
        }

        if (focused >= 0 && focused < decks.size()) {
            Deck selectedDeck = decks.get(focused);


            SimpleREST api = new SimpleREST(this);

            HashMap<String, String> postData = new HashMap<>();


            if (selectedDeck != null && selectedDeck.getSize() > 0) {

                postData.put("token", (String) userData.get("token"));
                postData.put("userid", String.valueOf(userData.get("serverid")));
                postData.put("collectionname", selectedDeck.getName());

                ArrayList<JSONObject> jsonStuff = new ArrayList<>();

                for (int k = 0; k < selectedDeck.getSize(); k++) {
                    Card card = selectedDeck.getCard(k);
                    HashMap<String, String> cardData = new HashMap<>();
                    cardData.put("fronttext", card.getFront());
                    cardData.put("backtext", card.getBack());

                    JSONObject obj = new JSONObject(cardData);

                    jsonStuff.add(obj);
                }

                JSONArray finalJSON = new JSONArray(jsonStuff);

                //Log.d("JSON",finalJSON.toString());

                postData.put("collectiondata", finalJSON.toString());
                postData.put("Content-Type", "application/json");

                final Context c = this;


                try {
                    api.PostJSON(Fetcher.CollectionURL(), postData, new VolleyCallback() {
                        @Override
                        public void onSuccessResponse(String result) throws JSONException {
                            JSONObject json = new JSONObject(result);

                            if (json.has("msg")) {
                                Toast.makeText(c, json.getString("msg"), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(c, "Error while uploading deck", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(String result) {
                            Toast.makeText(c, result, Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }else{
            Toast.makeText(this, "Select Valid Deck", Toast.LENGTH_LONG).show();
        }
    }
}
