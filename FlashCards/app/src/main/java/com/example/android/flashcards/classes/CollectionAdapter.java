package com.example.android.flashcards.classes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.flashcards.EditDeckActivity;
import com.example.android.flashcards.R;
import com.example.android.flashcards.interfaces.RecycleCallback;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Kevin on 2/28/2018.
 */


public class CollectionAdapter extends RecyclerView.Adapter<CollectionAdapter.ViewHolder> {

    private List<Card> collectionData;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView question;
        public TextView answer;
        public TextView cardnumber;
        public View layout;
        private RecycleCallback callbackListener;

        public ViewHolder(View v, RecycleCallback listener) {
            super(v);
            layout = v;
            cardnumber = v.findViewById(R.id.cardnum);
            question = v.findViewById(R.id.questionTxt);
            answer = v.findViewById(R.id.answerTxt);
            v.setOnClickListener(this);
            callbackListener = listener;
        }

        @Override
        public void onClick(View view) {
            callbackListener.onClick(view,getAdapterPosition());
        }
    }

    public CollectionAdapter(List<Card> map){
        collectionData = map;
    }

    @Override
    public CollectionAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        RecycleCallback callback = new RecycleCallback() {
            @Override
            public void onClick(final View cardview, final int pos) {
                AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext());
                builder.setMessage("Select Option for Card #"+pos);
                builder.setTitle("Card Options");

                builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (parent.getContext() instanceof EditDeckActivity) {
                            ((EditDeckActivity) parent.getContext()).onEditClicked(cardview);
                        }
                    }
                });

                builder.setNegativeButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RemoveCard(pos);
                        if (parent.getContext() instanceof EditDeckActivity) {
                            ((EditDeckActivity) parent.getContext()).onDeleteClicked(cardview);
                        }
                    }
                });

                builder.show();
            }
        };

        View v = inflater.inflate(R.layout.flashlayout,parent,false);
        ViewHolder vh = new ViewHolder(v,callback);
        return vh;
    }

    @Override
    public void onBindViewHolder(CollectionAdapter.ViewHolder holder, int i) {
        holder.cardnumber.setText(String.valueOf(i));
        holder.question.setText(collectionData.get(i).getFront());
        holder.answer.setText(collectionData.get(i).getBack());
    }

    public void DeleteData(){
        if (!collectionData.isEmpty()){
            collectionData.clear();
        }
    }

    public void AddCard(Card card){
        collectionData.add(card);
    }

    public void RemoveCard(int i){
        collectionData.remove(i);
        notifyItemRemoved(i);
    }

    @Override
    public int getItemCount() {
        return collectionData.size();
    }
}
