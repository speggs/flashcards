package com.example.android.flashcards.classes;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.flashcards.DeckDownload;
import com.example.android.flashcards.EditDeckActivity;
import com.example.android.flashcards.R;
import com.example.android.flashcards.interfaces.RecycleCallback;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Kevin on 2/28/2018.
 */


public class DeckAdapter extends RecyclerView.Adapter<DeckAdapter.ViewHolder> {

    private List<OnlineDeck> collectionData;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView numOfCards;
        public TextView deckName;
        public TextView author;
        public int serverid;
        public View layout;
        private RecycleCallback callbackListener;

        public ViewHolder(View v, RecycleCallback listener) {
            super(v);
            layout = v;
            numOfCards = v.findViewById(R.id.deck_amount);
            deckName = v.findViewById(R.id.deck_name);
            author = v.findViewById(R.id.deck_author);
            v.setOnClickListener(this);
            callbackListener = listener;
        }

        @Override
        public void onClick(View view) {
            callbackListener.onClick(view,getAdapterPosition());
        }
    }

    public DeckAdapter(List<OnlineDeck> map){
        collectionData = map;
    }

    @Override
    public DeckAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.decklayout,parent,false);

        RecycleCallback callback = new RecycleCallback() {
            @Override
            public void onClick(final View cardview, final int pos) {
                AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext());
                builder.setMessage("Download this deck?");
                builder.setTitle("Download");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (parent.getContext() instanceof DeckDownload) {
                            ((DeckDownload) parent.getContext()).DownloadDeck(collectionData.get(pos));
                        }
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                builder.show();
            }
        };

        ViewHolder vh = new ViewHolder(v,callback);
        return vh;
    }

    @Override
    public void onBindViewHolder(DeckAdapter.ViewHolder holder, int i) {
        holder.numOfCards.setText(String.format("%d",collectionData.get(i).GetDeckAmount()));
        holder.author.setText("By "+collectionData.get(i).GetDeckAuthor());
        holder.deckName.setText(collectionData.get(i).GetDeckName());
        holder.serverid = collectionData.get(i).GetCollectionID();
    }

    public void DeleteData() {
        if (!collectionData.isEmpty()) {
            collectionData.clear();
        }
    }

    @Override
    public int getItemCount() {
        return collectionData.size();
    }
}
