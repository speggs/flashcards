package com.example.android.flashcards;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.android.flashcards.classes.Card;
import com.example.android.flashcards.classes.Deck;
import com.example.android.flashcards.classes.FCApplication;

import org.w3c.dom.Text;

/*
*   Activity for Solo play allows user to go through deck, shuffle and flip cards
*
 */
//TODO Maybe a small indicator in a corner to show weather the user is viewing
//the front or back of a card
public class SoloPlayActivity extends AppCompatActivity {
    Deck loadedDeck;
    int cardNum = 0;
    boolean isBack = false;
    TextView cardView;
    TextView cardAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String sDeck;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solo_play);
        Intent intentExtras = getIntent();
        loadedDeck = new Deck();
       // tv.setText("sDeck");
        //Get deck name from extras and load the named deck
        if(intentExtras.hasExtra("sDeck")){
            sDeck = intentExtras.getStringExtra("sDeck");
            loadedDeck.load(this, sDeck);
            TextView deckNameTV = findViewById(R.id.deckNameView);
            deckNameTV.setText(sDeck);

            if (loadedDeck.getCard(0) != null){
                cardView = findViewById(R.id.cardview);
                cardAmount = findViewById(R.id.numofCards);
                cardView.setText(loadedDeck.getCard(0).getFront());
                cardAmount.setText("0/"+(loadedDeck.getSize() - 1));
            }
        }
        ((FCApplication)this.getApplication()).incrementGamesPlayed();
        ((FCApplication)this.getApplication()).incrementGamesWon();
    }

    public void onFlipcard(View v){
        Card ourCard = loadedDeck.getCard(cardNum);
        if (!isBack && ourCard != null){
            cardView.setText(ourCard.getBack());
            isBack = true;
        }else if(isBack && ourCard != null){
            cardView.setText(ourCard.getFront());
            isBack = false;
        }

        ObjectAnimator flip = ObjectAnimator.ofFloat(cardView,"rotationX",270f,360f);
        flip.setDuration(300);
        flip.start();
    }

    /*
    * Loads the ith card face up into the views
    * No bounds checking so be careful
     */
    private void loadCard(int i){
        Card ourCard = loadedDeck.getCard(i);
        cardView.setText(ourCard.getFront());
        cardAmount.setText(i +"/" + (loadedDeck.getSize() - 1));
    }

    public void onNextCard(View v){
        if(loadedDeck.getSize() - 1 > cardNum){
            cardNum++;
            loadCard(cardNum);
        }
    }

    public void onPrevCard(View v){
        if(cardNum > 0){
            cardNum--;
            loadCard(cardNum);
        }
    }

    public void onShuffleCard(View v){
        loadedDeck.shuffle();
        cardNum = 0;
        loadCard(cardNum);
    }

    public void onMenuClicked(View v){
        Intent menu = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(menu);
    }
}
