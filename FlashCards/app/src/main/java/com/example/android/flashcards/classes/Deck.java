package com.example.android.flashcards.classes;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by SPeggs on 2/17/2018.
 * Class for a deck of Cards
 * File name for a deck is deckname.deck
 * Save file format:
 * <DeckName>
 *     <Card>
 *         <front>front</front>
 *         <back>back</back>
 *     </Card>
 *     ...
 * </DeckName>
 */

public class Deck {
    public String deckName;
    public ArrayList<Card> deck;

    public Deck(){
        deckName = "New Deck";
        deck = new ArrayList<Card>();
    }

    public Deck(String name, ArrayList<Card> cards){
        deckName = name;
        deck = cards;
    }

    public void addCard(Card c){
        deck.add(c);
    }

    public void removeCard(int i){
        deck.remove(i);
    }

    public Card getCard(int i){
        Card ret = deck.get(i);
        return ret;
    }

    public int getSize(){
        return deck.size();
    }

    public void setName(String s, Context context){
        context.deleteFile(deckName + ".deck");
        deckName = s;
        save(context);
    }

    public String getName(){
        return deckName;
    }

    //Permanent shuffle, will effect save state etc
    public void shuffle(){
        Collections.shuffle(deck);
    }

    public void deleteDeck(Context context){
        context.deleteFile(deckName + ".deck");
    }
    /*
    *   Saves the deck to a file called "<DeckName>.deck"
     */
    public void save(Context context){
        String saveString = "";
        saveString += "<"+deckName+">\n";
        for(int i = 0; i < deck.size(); i++){
            //  <card>
            //      <front>front</back>
            //      <back>back</back>
            //  </card>
            saveString +="\t<card>\n" +
                    deck.get(i).saveString() +"" +
                    "\t</card>\n";
        }
        saveString += "</"+deckName+">";
        writeToFile(saveString, context);
    }

    private void writeToFile(String data,Context context) {
        String savePath = context.getFilesDir().toString();
        context.deleteFile(deckName + ".deck");
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(deckName + ".deck", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /*
    * loads <deckName>.txt from the private application folder
    * return true if successful else false
     */
    public boolean load(Context context, String deckToLoad){
        String deckString = "";
        deckString = readFromFile(context, deckToLoad);
        return true;
    }


    /*
    * Opens <deckToLoad>.txt from private application folder, parses it and loads it into the deck
     */
    private String readFromFile(Context context, String deckToLoad) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(deckToLoad + ".deck");

            if ( inputStream != null ) {
                int splitIndex = 8;
                String front = "Card Front";
                String back = "Card Back";
                String tempString = "";
                Card tempCard = new Card();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                //Parse file, first line is the deck name
                receiveString = bufferedReader.readLine();
                if(!receiveString.contains("<"+ deckToLoad +">")){
                    inputStream.close();
                    return ret;
                }else{
                    deckName = deckToLoad;
                }
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    if(receiveString.startsWith("\t<card>")){
                        tempCard = new Card();

                    }
                    else if(receiveString.startsWith("\t\t<front>")){
                        tempCard.setFront(receiveString.substring(9,receiveString.length() - 8));
                    }
                    else if(receiveString.startsWith("\t\t<back>")){
                        tempCard.setBack(receiveString.substring(8,receiveString.length() - 7));
                        deck.add(tempCard);
                    }else if(receiveString.startsWith("\t</card>")){
                    }
                    else {//format check
                        inputStream.close();
                        return ret;
                    }
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }
}
